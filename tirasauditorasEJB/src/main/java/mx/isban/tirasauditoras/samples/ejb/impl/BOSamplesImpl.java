package mx.isban.tirasauditoras.samples.ejb.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.tirasauditoras.samples.beans.CicsBean;
import mx.isban.tirasauditoras.samples.beans.ResultSamplesBean;
import mx.isban.tirasauditoras.samples.beans.SamplesBean;
import mx.isban.tirasauditoras.samples.dao.DAOSamplesCics;
import mx.isban.tirasauditoras.samples.dao.DAOSamplesDatabase;
import mx.isban.tirasauditoras.samples.dao.DAOSamplesMq;
import mx.isban.tirasauditoras.samples.dao.impl.DAOSamplesEjecutor;
import mx.isban.tirasauditoras.samples.ejb.BOSamples;
import mx.isban.agave.auditoria.Auditoria;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.AuditAdminBean;
import mx.isban.agave.commons.beans.AuditTransBean;
import mx.isban.agave.commons.beans.BitacoraBean;
import mx.isban.agave.commons.beans.MessageBean;
import mx.isban.agave.commons.exception.AuditoriaException;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.commons.exception.MensajeClientException;
import mx.isban.agave.mensajeria.MensajeClient;

@Stateless
@Remote(BOSamples.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOSamplesImpl extends Architech implements BOSamples {

	/** Serial version UID de la clase */
	private static final long serialVersionUID = 6711822210144483632L;
	
	@EJB
	private DAOSamplesDatabase daoSamplesDatabase;

	@EJB
	private DAOSamplesMq daoSamplesMq;

	@EJB
	private DAOSamplesCics daoSamplesCics;


	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoIsbanDataAccessDatabase(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoIsbanDataAccessDatabase(SamplesBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoIsbanDataAccessDatabase");
		ResultSamplesBean result = null;
		try {
			result = daoSamplesDatabase.usoIsbanDataAccessDatabase(valor, asb);
		} catch (ExceptionDataAccess e) {
			error("Error:"+e.getMessage());
			showException(e);
			throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
		}
		
		return result;
	}
	
	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoIsbanDataAccessMq(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoIsbanDataAccessMq(SamplesBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoIsbanDataAccessMq");

		ResultSamplesBean result = null;
		try {
			result = daoSamplesMq.usoIsbanDataAccessMq(valor, asb);
		} catch (ExceptionDataAccess e) {
			error("Error:"+e.getMessage());
			showException(e);
			throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
		}
		
		return result;
	}

	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoIsbanDataAccessCics(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public CicsBean usoIsbanDataAccessCics(CicsBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoIsbanDataAccessMq");

		CicsBean result = null;
		try {
			result = daoSamplesCics.usoIsbanDataAccessCics(valor, asb);
		} catch (ExceptionDataAccess e) {
			error("Error:"+e.getMessage());
			showException(e);
			throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
		}
		
		return result;
	}

	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoMensajeria(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoMensajeria(SamplesBean valor, ArchitechSessionBean sessionBean) throws BusinessException {
		debug("Ejecutando usoMensajeria");
		
		this.setArchitechBean(sessionBean);
        // Se obtiene un cliente de mensajeria.
        MensajeClient client = new MensajeClient(this.getLoggingBean());

        try {
            // Se crea el mensaje a enviar.
            MessageBean message = new MessageBean();
            message.setLogginBean(this.getLoggingBean());
            message.setDaoEjecutor(DAOSamplesEjecutor.class.getCanonicalName());
            message.setObjetoEjecutor(new ResultSamplesBean());

            // Se envia el mensaje.
            client.enviaMensaje(message);
        } catch (MensajeClientException e) {
			showException(e);
            throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
        }
        return null;

	}
	
	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoAuditoriaAdmin(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoAuditoriaAdmin(SamplesBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoAuditoriaAdmin");

		Date fecha = new Date();
        String hora = "auditAdminHora";
        String ipTerm = "auditAdminIpTerm";
        String canAplic = "auditAdminCanAplic";
        String usuario = this.getLoggingBean().getUsuario();
        String idInstaWeb = "auditAdminIdInstaWeb";
        String hostName = "auditAdminHostName";
        String datoAfectado = "";
        String tablaAfectada = "";

        AuditAdminBean auditAdminBean = new AuditAdminBean(fecha, hora, ipTerm, canAplic, usuario, "idSesion", idInstaWeb, hostName, datoAfectado, tablaAfectada);
        auditAdminBean.setCodOperacion("auditAdminCodOperacion");
        auditAdminBean.setServTrans("auditAdminServTrans");
        auditAdminBean.setIdToken("auditAdminIdToken");
        auditAdminBean.setTipoOperacion("auditAdminTipoOperacion");
        auditAdminBean.setDatoFijo("auditAdminDatoFijo");
        auditAdminBean.setDatoAfectado("auditAdminDatoAfectado");
        auditAdminBean.setTablaAfectada("auditAdminTablaAfectada");
        auditAdminBean.setValAnterior("auditAdminValAnterior");
        auditAdminBean.setValNuevo("auditAdminValNuevo");
        auditAdminBean.setIdOperacion(Long.valueOf("100"));

        try {
            Auditoria.grabaAuditoria(auditAdminBean, this.getLoggingBean());
        } catch (AuditoriaException e) {
			showException(e);
            throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
        }
        
        return null;

	}
	
	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoAuditoriaTrans(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoAuditoriaTrans(SamplesBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoAuditoriaTrans");

        Date fecha = new Date();
        String hora = "auditTransHora";
        String ipTerm = "auditTransIpTerm";
        String canAplic = "auditTransCanAplic";
        String idInstaWeb = "auditTransIdInstaWeb";
        String hostName = "auditTransHostName";
        String codCliente = "auditTransCodCliente";

        AuditTransBean auditTransBean = new AuditTransBean(fecha, hora, ipTerm, canAplic, "idSesion", idInstaWeb, hostName, codCliente);
        auditTransBean.setIdOperacion(Long.valueOf("0"));
        auditTransBean.setDescOperacion("auditTransDescOperacion");
        auditTransBean.setReferencia("auditTransReferencia");
        auditTransBean.setServTrans("auditTransServTrans");
        auditTransBean.setIdToken("auditTransIdToken");
        auditTransBean.setUsuario(this.getLoggingBean().getUsuario());
        auditTransBean.setFechaProgramada(new Date());
        auditTransBean.setTipoMoneda("auditTransTipoMoneda");
        auditTransBean.setNumTitulos(Long.valueOf("0"));
        auditTransBean.setMonto(Double.valueOf("0.00"));
        auditTransBean.setTipoCambio(Double.valueOf("0.00"));
        auditTransBean.setCodError("auditTransCodError");
        auditTransBean.setDescError("auditTransDescError");
        auditTransBean.setBancoDestino("auditTransBancoDestino");
        auditTransBean.setContrato(this.getLoggingBean().getCuentaOContrato());
        auditTransBean.setCuentaOrigen("auditTransCuentaOrigen");
        auditTransBean.setCuentaDestino("auditTransCuentaDestino");
        auditTransBean.setFechaAplicacion(new Date());
        auditTransBean.setEstatus("auditTransEstatus");
        auditTransBean.setNomArchivo("auditTransNomArchivo");

        try {
            Auditoria.grabaAuditoria(auditTransBean, this.getLoggingBean());
        } catch (AuditoriaException e) {
			showException(e);
            throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
        }

        return null;

	}
	
	/*
	 * (non-Javadoc) 
	 * @see mx.isban.tirasauditoras.principal.ejb.BOSamples#usoAuditoriaBitacora(mx.isban.tirasauditoras.samples.beans.BeanSamples, mx.isban.agave.commons.beans.ArchitechSessionBean) 
	 */
	@Override
	public ResultSamplesBean usoAuditoriaBitacora(SamplesBean valor, ArchitechSessionBean asb) throws BusinessException {
		debug("Ejecutando usoAuditoriaBitacora");

        BitacoraBean bitacoraBean = new BitacoraBean();
        bitacoraBean.setUsuario(this.getLoggingBean().getUsuario());
        bitacoraBean.setContrato(this.getLoggingBean().getCuentaOContrato());
        bitacoraBean.setIpTerm(this.getLoggingBean().getIp());
        bitacoraBean.setTipoOperacion("bitacoraTipoOperacion");
        bitacoraBean.setCodOperacion("bitacoraCodOperacion");
        bitacoraBean.setIdOperacion("bitacoraFolioOperacion");
        bitacoraBean.setCodCliente("bitacoraFolioUsuario");
        bitacoraBean.setEstatus("bitacoraEstatus");
        bitacoraBean.setTipoDocumento("bitacoraTipoDocumento");
        bitacoraBean.setDocumento("bitacoraDocumento");
        bitacoraBean.setIdSesion("idSession");
        bitacoraBean.setIdToken("bitacoraToken");
        bitacoraBean.setIdInstaWeb("bitacoraInstancia");
        bitacoraBean.setHostName("bitacoraHostName");
        bitacoraBean.setCampo1("bitacoraCampo1");
        bitacoraBean.setCampo2("bitacoraCampo2");
        bitacoraBean.setVarda1("bitacoraVarda1");
        bitacoraBean.setVarda2("bitacoraVarda2");
        bitacoraBean.setVarda3("bitacoraVarda3");
        bitacoraBean.setVarda4("bitacoraVarda4");
        bitacoraBean.setVarda5("bitacoraVarda5");
        bitacoraBean.setIdAplicacion("bitacoraIdAplicacion");
        bitacoraBean.setUsuario390("bitacoraUsuario390");

        try {
            Auditoria.guardaBitacora(bitacoraBean, this.getLoggingBean());
        } catch (AuditoriaException e) {
			showException(e);
            throw new BusinessException(BOSamplesImpl.class.getName(), e.getCode(), e.getMessage());
        }

        return null;

	}

	/**
	 * 
	 * @param daoSamplesDatabase Servicio de acceso a datos de base de datos
	 */
	public void setDaoSamplesDatabase(DAOSamplesDatabase daoSamplesDatabase) {
		this.daoSamplesDatabase = daoSamplesDatabase;
	}

	/**
	 * 
	 * @param daoSamplesDatabase Servicio de acceso a datos de MQ
	 */
	public void setDaoSamplesMq(DAOSamplesMq daoSamplesMq) {
		this.daoSamplesMq = daoSamplesMq;
	}

	/**
	 * 
	 * @param daoSamplesDatabase Servicio de acceso a datos de CICS
	 */
	public void setDaoSamplesCics(DAOSamplesCics daoSamplesCics) {
		this.daoSamplesCics = daoSamplesCics;
	}

}