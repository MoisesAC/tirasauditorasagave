package mx.isban.tirasauditoras.samples.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import mx.isban.agave.cache.GlobalCache;
import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.configuracion.ConfiguracionConfig;
import mx.isban.agave.configuracion.ConfiguracionConfigs;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import mx.isban.tirasauditoras.samples.beans.CicsBean;
import mx.isban.tirasauditoras.samples.beans.SamplesBean;
import mx.isban.tirasauditoras.samples.ejb.BOSamples;



@Controller
@RequestMapping("/samples")
public class SamplesController  extends Architech{


	/** Serial version UID de la clase */
	private static final long serialVersionUID = -2288138134885533326L;

	@Autowired
	private BOSamples boSamples;

	/** Fuente de mensajes properties */
	@Autowired
	private MessageSource messageSource;
	
	/** Constante de pagina de inicio */
	private static final String LANDING_PAGE = "public/samples/SamplesMenu";
	/** Constante de pagina de inicio */
	private static final String CICS_TEST_PAGE = "public/samples/CicsTest";

	/**
	 * Landing a menu principal de menu de ejemplos
	 * @return ModelAndView de menu principal
	 * @throws Exception
	 */
	@RequestMapping(value = {"","/","/inicio"}, method = RequestMethod.GET)
    public ModelAndView viewLogin(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(LANDING_PAGE);
		return mav;
    }

	/**
	 * Landing a menu pantalla de ejecucion de prueba de CICS
	 * @return ModelAndView de test de CICS
	 * @throws Exception
	 */
	@RequestMapping(value = "/cicsTest", method = RequestMethod.GET)
    public ModelAndView viewCicsTest(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView();
		CicsBean cb = new CicsBean();
		mav.setViewName(CICS_TEST_PAGE);
        model.put("cicsForm", cb);
        return mav;
    }
	
	/**
	 * Accion para ejemplo de uso de cmpConfiguracion
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoConfig.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoConfig() {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
		
        info("-----Sample de uso de cmpConfiguracion------");

		info("Impresion de parametro de configuracion global MAXIMO_REGISTROS en archivo tirasauditorasArqAgave_WEB.xml ");
		info(ConfiguracionConfigs.getInstance().getConfig(ConfiguracionConfig.NOMBRE_APLICACION));

		
        res.put("status", "SUCCESS");

        return res;
	}
	

	/**
	 * Accion para ejemplo de uso de cmpLogging
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoLogging.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoLogging() {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
		
		debug("-----Sample de uso de cmpLogging en nivel de debug------");

		info("-----Sample de uso de cmpLogging en nivel de info------");

		warn("-----Sample de uso de cmpLogging en nivel de warn------");
		
		error("-----Sample de uso de cmpLogging en nivel de error------");
		
        res.put("status", "SUCCESS");

        return res;
	}
	


	/**
	 * Accion para ejemplo de uso de cmpIsbanDataAccess
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoIsbanDataAccessDatabase.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoIsbanDataAccessDatabase()  throws Exception {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
        SamplesBean bs = new SamplesBean();
        bs.setParamA("0014");
        boSamples.usoIsbanDataAccessDatabase(bs, getArchitechBean());
		
        res.put("status", "SUCCESS");
        
        return res;
	}
	


	/**
	 * Accion para ejemplo de uso de cmpIsbanDataAccess
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoIsbanDataAccessMq.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoIsbanDataAccessMq()  throws Exception {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
        SamplesBean bs = new SamplesBean();
        bs.setParamA("0014");
        
        boSamples.usoIsbanDataAccessMq(bs, getArchitechBean());
		
        res.put("status", "SUCCESS");

        return res;
	}

	/**
	 * Accion para ejemplo de uso de cmpIsbanDataAccess
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoIsbanDataAccessCics.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoIsbanDataAccessCics(@Valid @ModelAttribute("samplesForm") CicsBean bs, 
			BindingResult result, Map<String, Object> model)  throws Exception {

		List<String> list = new ArrayList<String>();
		JSONObject res = new JSONObject();
		
        if(result.hasErrors()){
            res.put("status", "FAIL");
            
			for (FieldError fieldError : result.getFieldErrors()) {
				String message = fieldError.getDefaultMessage();
				this.debug("Locale: "+LocaleContextHolder.getLocale());
				if (messageSource != null) {
					message = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
				}
				
				list.add(message);
			}

            res.put("respuesta", list);
        }else{
        	
        	CicsBean br = boSamples.usoIsbanDataAccessCics(bs, getArchitechBean());

    		res.put("status", "SUCCESS");
    		res.put("respuesta", br.getRespuesta());
        }
		

        return res;
	}

	/**
	 * Accion para ejemplo de uso de cmpMensajeria
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoMensajeria.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoMensajeria()  throws Exception {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
        SamplesBean bs = new SamplesBean();
        bs.setParamA("0014");
        
        boSamples.usoMensajeria(bs, getArchitechBean());

        res.put("status", "SUCCESS");
        
        return res;
	}
	
	/**
	 * Accion para ejemplo de uso de cmpAuditoria
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoAuditoria.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoAuditoria()  throws Exception {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
        SamplesBean bs = new SamplesBean();
        bs.setParamA("0014");
        
        boSamples.usoAuditoriaAdmin(bs, getArchitechBean());

        boSamples.usoAuditoriaBitacora(bs, getArchitechBean());
        
        boSamples.usoAuditoriaTrans(bs, getArchitechBean());
        
        res.put("status", "SUCCESS");
        
        return res;
	}

	/**
	 * Accion para ejemplo de uso de cmpMensajeria
	 * @return objeto json indicando status de accion
	 */
	@RequestMapping(path = "usoCache.do", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject usoCache()  throws Exception {
		JSONObject res = new JSONObject();
        res.put("status", "FAIL");
        SamplesBean bs = new SamplesBean();
        bs.setParamA("0014");


        String llave = "ArregloDeBytes01";
        byte[] dato = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
        boolean result = GlobalCache.getInstance(this.getLoggingBean()).guardaDato(llave, dato);
        debug("Resultado de guardado en Cache:"+ result);


        //Consulta datos en cache:
        Object resultObten = GlobalCache.getInstance(this.getLoggingBean()).obtenDato(llave);
        debug("Resultado de consulta de Cache:"+ resultObten);


        res.put("status", "SUCCESS");
        
        return res;
	}
	
	public void setBoSamples(BOSamples boSamples) {
		this.boSamples = boSamples;
	}
	
}
