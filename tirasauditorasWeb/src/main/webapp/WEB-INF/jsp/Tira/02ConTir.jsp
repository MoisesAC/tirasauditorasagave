<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../../../css/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<%-- AUTOR: Ing. David Aguilar G�mez.--%>
<%-- CREACION: 01/02/2006 --%>
	<SCRIPT src="../../../js/calendario.js"></SCRIPT>
	<SCRIPT src="../../../js/validaciones.js"></SCRIPT>
	<SCRIPT language="Javascript">
		// Field Properties for Validations
			var valida_fecha = new Array('fecha','Fecha','D',true,0,0);
			var valida_pventa = new Array('pventa','Punto de Venta','N',true,4,4);
			var valida_nooperacion = new Array('nooperacion','No. de Operaci�n','N+',true,0,7);
			var valida_cuenta1 = new Array('cuenta1','Cuenta','N',true,2,2);
			var valida_cuenta2 = new Array('cuenta2','Cuenta','N',true,9,9);
			var valida_usuario = new Array('usuario','Usuario','A',true,0,7);
			var valida_horaini = new Array('horaini','Hora Inicial','H',true,2,2);
			var valida_horafin = new Array('horafin','Hora Final','H',true,2,2);
			var valida_minutoini = new Array('minutoini','Hora Inicial','M',true,2,2);
			var valida_minutofin = new Array('minutofin','Hora Final','M',true,2,2);
			var valida_importeini = new Array('importeini','Importe Inicial','F+',true,0,19);
			var valida_importefin = new Array('importefin','Importe Final','F+',true,0,19);
			var valida_idats = new Array('cboIdAts','Identificador de Ats','A',false,1,7);

		function valida(campo,propiedades)
		{
			var lMensaje="";
      var lTempMssg=assertField(campo, propiedades);
      if (lTempMssg.length>1)
      	lMensaje+=assertField(campo, propiedades)+"\n";
      if(propiedades[0]=='importefin' && lMensaje.length=='0')
      {
      	if(document.frmConTir.importeini.value.length>0)
      	{
      		if(parseFloat(campo.value)< parseFloat(document.frmConTir.importeini.value))
      			lMensaje+="El Importe Final debe de ser igual � mayor al Importe Inicial\n";
        }
			}
      if(propiedades[0]=='horafin' && lMensaje.length=='0')
      {
      	if(document.frmConTir.horaini.value.length>0)
      	{
      		if(new Number(campo.value)<new Number(document.frmConTir.horaini.value))
      			lMensaje+="La Hora Final debe de ser mayor a la Hora Inicial\n";
				}
			}
			if(propiedades[0]=='minutofin' && lMensaje.length=='0')
			{
				if(document.frmConTir.horaini.value.length>0 && document.frmConTir.horafin.value.length>0 && document.frmConTir.minutoini.value.length>0)
				{
					if(new Number(document.frmConTir.horafin.value)<new Number(document.frmConTir.horaini.value) || (new Number(document.frmConTir.horafin.value)==new Number(document.frmConTir.horaini.value) && new Number(campo.value)<=new Number(document.frmConTir.minutoini.value)))
						lMensaje+="La Hora Final debe de ser mayor a la Hora Inicial\n";
				}
			}
	    if(propiedades[0]=='cuenta2' && lMensaje.length=='0')
	    {
	    	if(document.frmConTir.cuenta1.value.length>0 && document.frmConTir.cuenta2.value.length>0 && !validaDigito(document.frmConTir.cuenta1,document.frmConTir.cuenta2))
	    	{
	   			lMensaje+="Error en el digito de la cuenta\n";
	    	}
	    }
	    if (lMensaje.length>0)
	    {
	    	alert(lMensaje);
	      //campo.focus();
	      //campo.select();
	      return false;
	    }
	    else
	    {
	    	return true;
	    }
		}
		function limpia()
		{
			document.frmConTir.fecha.value = '';
			document.frmConTir.pventa.value = '';
			document.frmConTir.cboSuc.value = 0;
			document.frmConTir.seleccion[0].checked = true;
			fselec(document.frmConTir.seleccion);
			document.frmConTir.cboOrigen.value = "";

			document.getElementById("cboIdAts").innerHTML = "";
			addItem("cboIdAts", "Seleccione Id...", "0");


			var select = document.getElementById("cbtoperacion");
			select.innerHTML = "";

			addItem("cbtoperacion", "Seleccione tipo...", "0");
			for(var i=0;i<operaText.length;i++)
			{
				addItem("cbtoperacion", operaText[i], operaValue[i]);
			}

		}
		function busca()
		{
			var bandera = 1;
			var j;
	    for(j=0;j<document.frmConTir.seleccion.length;j++)
	    {
	    	if(document.frmConTir.seleccion[j].checked)
	    		break;
	    }
			if(!valida(document.frmConTir.fecha,valida_fecha))
				bandera = '0';
			else
			{
				if(!valida(document.frmConTir.pventa,valida_pventa) || document.frmConTir.cboSuc.selectedIndex==0)
				{
					document.frmConTir.pventa.focus();
		     	document.frmConTir.pventa.select();
		     	bandera = '0';
				}
				else
				{
					switch(document.frmConTir.seleccion[j].value)
					{
						case '1':
							if(document.frmConTir.cbtoperacion.selectedIndex == 0)
							{
								bandera = '0';
								alert('No es un Tipo de Operaci�n valido');
							}
						break;
						case '2':
							if(!valida(document.frmConTir.nooperacion,valida_nooperacion))
								bandera = '0';
						break;
						case '3':
							if(!valida(document.frmConTir.cuenta1,valida_cuenta1))
								bandera = '0';
							else
								if(!valida(document.frmConTir.cuenta2,valida_cuenta2))
									bandera = '0';
						break;
						case '4':
							if(!valida(document.frmConTir.usuario,valida_usuario))
								bandera = '0';
						break;
						case '5':
							if(!valida(document.frmConTir.horaini,valida_horaini) || !valida(document.frmConTir.minutoini,valida_minutoini))
								bandera = '0';
							else
								if(!valida(document.frmConTir.horafin,valida_horafin) || !valida(document.frmConTir.minutofin,valida_minutofin))
									bandera = '0';
						break;
						case '6':
							if(!valida(document.frmConTir.importeini,valida_importeini))
								bandera = '0';
							else
								if(!valida(document.frmConTir.importefin,valida_importefin))
									bandera = '0';
						break;
						case '7':
							if(document.frmConTir.cboIdAts.value == ""){
								bandera = '0';
								alert('Es necesario un Identificador de ATS');
							}
						break;
					}
				}
			}
			if(bandera=='1')
			{
				document.frmConTir.accion.value = 'buscar';
				document.frmConTir.submit();
			}
		}
	</SCRIPT>
</HEAD>

<BODY>
<!-- Se cambia accion: "../servlet/../TiraServlet" -> "TiraServlet.do"
			para cambio servlet -> Controller	-->
	<form id="frmConTir" name="frmConTir" method="post" action="TiraServlet.do">
	<table width="100%" border="0">
	  <tr>
	    <td>
	      <p class="ATitulocolor">Consulta de Tira Auditoria</p>
	      <TABLE width="90%" border="0" cellspacing="0" align="center">
	        <TR>
	          <TD class="Atexencabezado" align="right">Fecha:</TD>
	          <TD>
	            <INPUT type="text" name="fecha" tabindex="1" size="11" maxlength="10" class="Atextittab" readonly>
				<A href="javascript:cal1.popup()">
					<IMG name="Calendario" border="0" src="../../../img/calendario.gif">
				</A>
	          </TD>
	          <TD class="Atexencabezado" align="right">&nbsp;Punto de Venta:</TD>
	          <TD colspan="2">
	          	&nbsp;
	          	<INPUT type="text" name="pventa" size="5" class="Atextittab" maxlength="4" onblur="javascript:valida(this,valida_pventa);setSuc(this);">
			  	<SELECT name="cboSuc" class="Atextittab" disabled>
					<OPTION value="0">&nbsp;</OPTION>
					<%
						java.util.ArrayList sucursales = com.santander.tira.TiraClase.allSucursal();
						if(sucursales.size()>0)
						{
							for(int i=0;i<sucursales.size();i++)
							{
								com.santander.tira.TiraValue valor = (com.santander.tira.TiraValue)sucursales.get(i);
					%>
					<OPTION value="<%=valor.getPVenta()+"-"+valor.getTraSabado()%>"><%=valor.getDescripcion()%></OPTION>
					<%
							}
						}
					%>
				</SELECT>
	          </TD>
	         </TR>
	         <TR>
	          <TD class="Atexencabezado" align="right">&nbsp;Origen:</TD>
	          <TD colspan="1">
			  	<SELECT name="cboOrigen" class="Atextittab" onchange="tipoOrigen(this.value);">
					<OPTION value="" selected>Todos</OPTION>
					<OPTION value="CE">Cajoneras</OPTION>
					<OPTION value="R">Reciclador</OPTION>
				</SELECT>
	          </TD>
	        </TR>
	        <TR>
	          <TD colspan="5">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="5" class="Atittabcenazu">&nbsp;Criterios de B�squeda</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
	          <TD align="right">
	          	<INPUT type="radio" name="seleccion" value="0" checked onclick="javascript:fselec(this);" id="esNinguno"></TD>
	          <TD colspan="4" class="Atexencabezado">&nbsp;Ninguno</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="1" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Tipo de Operaci&oacute;n</TD>
			  <TD align="left" colspan="3">
			  	<SELECT name="cbtoperacion" id="cbtoperacion" class="Atextittab" disabled onchange="javascript:document.frmConTir.dscoperacion.value = document.frmConTir.cbtoperacion[document.frmConTir.cbtoperacion.selectedIndex].text;" style="width:300px;">
					<OPTION value="0" selected>&nbsp;Seleccione tipo...</OPTION>
				</SELECT>
			  </TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="2" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;No. Operaci&oacute;n</TD>
			  <TD align="left" colspan="3">
			  	<INPUT type="text" name="nooperacion" size="10" class="Atextittab" disabled onblur="javascript:valida(this,valida_nooperacion);" maxlength="7">
			  </TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="3" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Cuenta</TD>
			  <TD align="left" colspan="3">
			  	<%--INPUT type="text" name="cuenta1" size="3" class="Atextittab" disabled onblur="javascript:valida(this,valida_cuenta1);" onkeydown="javascript:salta(this);" maxlength="3"--%>
			  	<INPUT type="text" name="cuenta1" size="2" class="Atextittab" disabled onkeyup="javascript:salta(this);" maxlength="2">
			  	<INPUT type="text" name="cuenta2" size="9" class="Atextittab" disabled onblur="javascript:valida(document.frmConTir.cuenta1,valida_cuenta1);valida(this,valida_cuenta2);" maxlength="9">
			  </TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="4" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Usuario</TD>
			  <TD align="left" colspan="3">
			  	<INPUT type="text" name="usuario" size="10" class="Atextittab" disabled onblur="javascript:valida(this,valida_usuario);" maxlength="7">
			  </TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="5" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Hora Inicial</TD>
			  <TD align="left">
			  	<INPUT type="text" name="horaini" size="2" class="Atextittab" disabled onblur="javascript:valida(this,valida_horaini);" maxlength="2">:
			  	<INPUT type="text" name="minutoini" size="2" class="Atextittab" disabled onblur="javascript:valida(this,valida_minutoini);" maxlength="2">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Hora Final</TD>
			  <TD>
			  	<INPUT type="text" name="horafin" size="2" class="Atextittab" disabled onblur="javascript:valida(this,valida_horafin);" maxlength="2">:
			  	<INPUT type="text" name="minutofin" size="2" class="Atextittab" disabled onblur="javascript:valida(this,valida_minutofin);" maxlength="2">
			  </TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="6" onclick="javascript:fselec(this);">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Importe Inicial</TD>
			  <TD align="left">
			  	<INPUT type="text" name="importeini" size="20" class="Atextittab" disabled onblur="javascript:valida(this,valida_importeini);" maxlength="16"/>
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Importe Final</TD>
			  <TD>
			  	<INPUT type="text" name="importefin" size="20" class="Atextittab" disabled onblur="javascript:valida(this,valida_importefin);" maxlength="16"/>
			  </TD>
			</TR>

			<TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="7" id="esIdAts" onclick="javascript:fselec(this);"/>
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Identificador de ATS</TD>
			  <TD align="left" colspan="3" >
			  	<SELECT name="cboIdAts" class="Atextittab" id="cboIdAts" disabled style="width:300px;">
					<OPTION value="0" selected>&nbsp;Seleccione Id...</OPTION>
			    </SELECT>
			  </TD>
			</TR>

			<TR>
			  <TD colspan="5" height="10" class="Atittabcenazu">&nbsp;</TD>
			</TR>
			<TR>
			  <TD colspan="5" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
			  <TD align="center" height="17" colspan="5">
				<A href="javascript:limpia();">
					<IMG name="btnlimpiar" border="0" src="../../../img/b_limpiar.gif"/></A>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				<A href="javascript:busca();">
					<IMG name="btnbuscar" border="0" src="../../../img/b_buscar.gif"/></A>
			  </TD>
			</TR>
		</TABLE>
        <p></p>
	    <p>&nbsp;</p>
	    </td>
	  </tr>
	</table>
	<INPUT type="hidden" name="accion" value="">
	<%
	java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("MM'/'dd'/'yyyy");
	%>
	<INPUT type="hidden" name="afecha" value="<%=formato.format(new java.util.Date())%>">
	<INPUT type="hidden" name="dscoperacion" value="">
	<INPUT type="hidden" name="nomsuc" value="">
	</form>
<SCRIPT language="JavaScript">
	document.frmConTir.cbtoperacion.length = operaText.length+1;
	for(var i=0;i<operaText.length;i++)
	{
		document.frmConTir.cbtoperacion[i+1].text  = operaText[i];
		document.frmConTir.cbtoperacion[i+1].value  = operaValue[i];
	}
	document.frmConTir.cbtoperacion.disabled = true;
	var cal1 = new calendario(document.frmConTir.fecha,document.frmConTir.afecha);
</SCRIPT>
</BODY>
</HTML>